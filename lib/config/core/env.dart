
import 'package:flutter/material.dart';
// import 'package:get/get.dart';

import 'app_component.dart';
import 'app_store_application.dart';
import 'di.dart';

enum EnvType {
  DEVELOPMENT,
  STAGING,
  PRODUCTION,
  TESTING
}

class Env {

  static late Env value;

  late String appName;
  late String baseUrl;
  late String tnc;
  late Color primarySwatch;
  EnvType environmentType = EnvType.DEVELOPMENT;

  // Database Config
  int? dbVersion = 1;
  String? dbName;

  Env() {
    value = this;
    _init();
  }

  void _init() async {
    WidgetsFlutterBinding.ensureInitialized();
    await DenpendencyInjection.init();

    var application = AppStoreApplication();
    await application.onCreate();
    runApp(AppComponent(application));
  }

}