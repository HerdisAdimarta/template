class StorageConstants {
  static String token = 'token';
  static String userId = 'userId';
  static String login = 'login';
  static String email = 'email';
  static String keepUrl = 'false';
  static String refresToken = 'refreshToken';
  static String userName = 'userName';
  static String status = 'status';
  static String hasLogined = 'hasLogined';
  static String resetPassKey = 'resetPassKey';

}
