import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:template/app/lang/translation_service.dart';
import 'package:template/app/theme/theme_data.dart';
import 'package:template/config/core/env.dart';

import 'app_pages.dart';
import 'app_provider.dart';
import 'app_store_application.dart';
import 'binding.dart';

class AppComponent extends StatefulWidget {
  final AppStoreApplication _application;

  AppComponent(this._application);

  @override
  State createState() {
    return AppComponentState();
  }
}

class AppComponentState extends State<AppComponent> {
  @override
  void dispose() async {
    super.dispose();
    await widget._application.onTerminate();
  }

  @override
  void initState() {
    super.initState();
    //    initPluginTest();
  }

  @override
  Widget build(BuildContext context) {
    Get.log(Env.value.appName);

    final myApp = GetMaterialApp(
      debugShowCheckedModeBanner: false,
      enableLog: Env.value.environmentType != EnvType.PRODUCTION,
      initialRoute: AppPages.initial,
      defaultTransition: Transition.noTransition,
      getPages: AppPages.routes,
      initialBinding: AppBinding(),
      smartManagement: SmartManagement.keepFactory,
      title: Env.value.appName,
      color: Env.value.primarySwatch,
      theme: ThemeConfig.lightTheme,
      locale: TranslationService.locale,
      fallbackLocale: TranslationService.fallbackLocale,
      translations: TranslationService(),
    );

    final appProvider =
    AppProvider(child: myApp, application: widget._application);
    return appProvider;
  }

}