import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'widget_button_fail.dart';
import 'widget_button_primary.dart';
import 'widget_button_secondary.dart';


class CustomAlert extends StatefulWidget {
  @override
  CustomAlertState createState() => CustomAlertState();
}

class CustomAlertState extends State<CustomAlert> {
  late BuildContext loading;
  late BuildContext success;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Container(
          decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10))),
          width: 10,
          height: 120,
          child: Container(
            margin: const EdgeInsets.all(15),
            child: Column(
              children: [
                const SpinKitThreeBounce(
                  color: Color(0xff003CBF),
                  size: 25,
                ),
                Container(
                  margin: const EdgeInsets.only(top: 15),
                  child: const Text("Loading ...",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 14.0,
                          height: 1.5,
                          fontWeight: FontWeight.w200)),
                ),
              ],
            ),
          )),
    );
  }

  void showDLoading(BuildContext context) {
    loading = context;
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return Dialog(
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Container(
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              width: 10,
              height: 120,
              child: Container(
                margin: const EdgeInsets.all(15),
                child: Column(
                  children: [
                    const SpinKitThreeBounce(
                      color: Color(0xFFB6E9E5),
                      size: 25,
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 15),
                      child: const Text("Loading ...",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14.0,
                              height: 1.5,
                              fontWeight: FontWeight.w200)),
                    ),
                  ],
                ),
              )),
        );
      },
    );
  }

  Future<void> showDialogWarning(
      BuildContext context, {
        String? title,
        String? msg,
        String? buttonPositif,
        String? buttonNegatif,
        Function? onPositif,
        Function? onNegatif,
      }) async {
    //
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          insetPadding: const EdgeInsets.all(15),
          child: Container(
              width: 200,
              decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xFFFFCB3C),),
                  borderRadius: const BorderRadius.all(Radius.circular(10))),
              child: Container(
                margin: const EdgeInsets.all(20),
                child: Wrap(
                  children: [
                    Column(
                      children: [
                        const Icon(Icons.warning_rounded,
                            size: 60, color: Color(0xFFFFCB3C)),
                        const SizedBox(
                          height: 5,
                        ),
                        title == null
                            ? Container()
                            : Text(title,
                            textAlign: TextAlign.center,
                            style: const TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                            )),
                        const SizedBox(
                          height: 15,
                        ),
                        msg == null
                            ? Container()
                            : Text(msg,
                            textAlign: TextAlign.center,
                            style: const TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                            )),
                        const SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                            width: double.infinity,
                            height: 35,
                            child: Row(
                              children: [
                                Expanded(
                                  child: ButtonSecondary(
                                    buttonText: buttonNegatif ?? 'Tutup',
                                    onpressed: () {
                                      if (onNegatif != null) {
                                        onNegatif();
                                      } else {
                                        Navigator.pop(context);
                                      }
                                    },
                                  ),
                                ),
                                const SizedBox(
                                  width: 15,
                                ),
                                Expanded(
                                  child: ButtonPrimary(
                                    buttonText: buttonPositif ?? 'Tutup',
                                    onpressed: () {
                                      if (onPositif != null) {
                                        onPositif();
                                      } else {
                                        Navigator.pop(context);
                                      }
                                    },
                                  ),
                                ),
                              ],
                            )),
                      ],
                    ),
                  ],
                ),
              )),
        );
      },
    );
  }

  Future<void> showDialogMessage(
      BuildContext context,
      bool status, {
        String? title,
        required String msg,
        String? buttonText,
        Function? onPress,
        bool? useImage,
      }) async {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          insetPadding: const EdgeInsets.all(15),
          child: Container(
              width: 200,
              decoration: BoxDecoration(
                  border: Border.all(
                      color: status ? const Color(0xFFB6E9E5) : const Color(0xFFFF3333)),
                  borderRadius: const BorderRadius.all(Radius.circular(10))),
              child: Container(
                margin: const EdgeInsets.all(20),
                child: Wrap(
                  children: [
                    Column(
                      children: [
                        useImage == null
                            ? Icon(status ? Icons.check : Icons.error,
                            size: 50,
                            color: status
                                ? const Color(0xFFB6E9E5)
                                : const Color(0xFFFF3333))
                            : Container(),
                        const SizedBox(
                          height: 5,
                        ),
                        title == null
                            ? Container()
                            : Text(title,
                            textAlign: TextAlign.center,
                            style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 17,
                            ),),
                        const SizedBox(
                          height: 15,
                        ),
                        Text(msg,
                            textAlign: TextAlign.center,
                            style: const TextStyle(
                              color: Colors.black,
                              fontSize: 17,
                            ),),
                        const SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                            width: 200,
                            height: 35,
                            child: status
                                ? ButtonPrimary(
                              buttonText: buttonText ?? 'Tutup',
                              onpressed: () {
                                if (onPress != null) {
                                  onPress();
                                } else {
                                  Navigator.pop(context);
                                }
                              },
                            )
                                : ButtonFail(
                              buttonText: buttonText ?? 'Tutup',
                              onpressed: () {
                                if (onPress != null) {
                                  onPress();
                                } else {
                                  Navigator.pop(context);
                                }
                              },
                            )),
                      ],
                    ),
                  ],
                ),
              )),
        );
      },
    );
  }

  Future<void> showDialogFailAutomaticallyDissmiss(
      BuildContext context,
      String msg, {
        required Function onClose,
        int duration = 1,
      }) async {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        Future.delayed(Duration(seconds: duration), () {
          onClose();
        });
        return Dialog(
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          insetPadding: const EdgeInsets.all(15),
          child: SingleChildScrollView(
            child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.red),
                    borderRadius: const BorderRadius.all(Radius.circular(10))),
                width: 200,
                child: Container(
                  margin: const EdgeInsets.all(15),
                  child: Column(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(top: 15, bottom: 15),
                        child: Column(
                          children: [
                            const Icon(Icons.error, size: 50, color: Colors.red),
                            const SizedBox(
                              height: 5,
                            ),
                            Text(msg,
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                    color: Colors.black,
                                    fontSize: 14.0,
                                    height: 1.5,
                                    fontWeight: FontWeight.w200)),
                          ],
                        ),
                      ),
                    ],
                  ),
                )),
          ),
        );
      },
    );
  }

  Future<void> showDialogFail(
      BuildContext context,
      String msg, {
        required Function onClose,
      }) async {
    //
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          insetPadding: const EdgeInsets.all(15),
          child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.red),
                  borderRadius: const BorderRadius.all(Radius.circular(10))),
              width: 200,
              child: Container(
                margin: const EdgeInsets.all(15),
                child: Wrap(
                  children: [
                    const SizedBox(
                        width: double.infinity,
                        child: Icon(Icons.error, size: 50, color: Colors.red)),
                    const SizedBox(
                      width: double.infinity,
                      height: 15,
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: Text(
                        msg,
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                            color: Colors.black,
                            fontSize: 14.0,
                            height: 1.5,
                            fontWeight: FontWeight.w200),
                      ),
                    ),
                    const SizedBox(
                      width: double.infinity,
                      height: 15,
                    ),
                    ButtonPrimary(
                      buttonText: 'Close',
                      onpressed: () {
                        onClose();
                      },
                    ),
                  ],
                ),
              )),
        );
      },
    );
  }

  Future<void> showDialogSuccessAutomaticallyDissmiss(
      BuildContext context,
      String msg, {
        required Function onClose,
      }) async {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        Future.delayed(const Duration(seconds: 1), () {
          onClose();
        });
        return Dialog(
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          insetPadding: const EdgeInsets.all(15),
          child: SingleChildScrollView(
            child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: const Color(0xFFB6E9E5)),
                    borderRadius: const BorderRadius.all(Radius.circular(10))),
                width: 200,
                child: Container(
                  margin: const EdgeInsets.all(15),
                  child: Column(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(top: 15, bottom: 15),
                        child: Column(
                          children: [
                            const Icon(Icons.check,
                                size: 50, color: Color(0xFFB6E9E5)),
                            const SizedBox(
                              height: 5,
                            ),
                            Text(msg,
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                    color: Colors.black,
                                    fontSize: 14.0,
                                    height: 1.5,
                                    fontWeight: FontWeight.w200)),
                          ],
                        ),
                      ),
                    ],
                  ),
                )),
          ),
        );
      },
    );
  }
}
