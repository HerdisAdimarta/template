// To parse this JSON data, do
//
//     final registerResponse = registerResponseFromJson(jsonString);

import 'dart:convert';

RegisterResponse registerResponseFromJson(String str) => RegisterResponse.fromJson(json.decode(str));

String registerResponseToJson(RegisterResponse data) => json.encode(data.toJson());

class RegisterResponse {
  int? code;
  bool? success;
  // String? message;
  Data? data;

  RegisterResponse({
    this.code,
    this.success,
    // this.message,
    this.data,
  });

  factory RegisterResponse.fromJson(Map<String, dynamic> json) => RegisterResponse(
    code: json["code"],
    success: json["success"],
    // message: json["message"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "success": success,
    // "message": message,
    "data": data?.toJson(),
  };
}

class Data {
  int? idUser;
  String? namaUser;
  String? email;
  String? accessToken;

  Data({
    this.idUser,
    this.namaUser,
    this.email,
    this.accessToken,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    idUser: json["id_user"],
    namaUser: json["nama_user"],
    email: json["email"],
    accessToken: json["access_token"],
  );

  Map<String, dynamic> toJson() => {
    "id_user": idUser,
    "nama_user": namaUser,
    "email": email,
    "access_token": accessToken,
  };
}
