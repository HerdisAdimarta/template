import 'package:flutter/material.dart';
import 'package:template/config/core/env.dart';

void main() => Development();

class Development extends Env {
  final String appName = 'Dev template';
  final String baseUrl = 'https://google.com';
  final Color primarySwatch = Colors.pink;
  EnvType environmentType = EnvType.DEVELOPMENT;

  final String dbName = 'template-Dev.db';
}