
import 'package:get/get.dart';
import 'package:template/app/network/api/api_provider.dart';
import 'package:template/app/network/api/api_repository.dart';

class AppBinding extends Bindings {
  @override
  void dependencies() async {
    Get.put(APIProvider(), permanent: true);
    Get.put(APIRepository(apiProvider: APIProvider()), permanent: true);
  }
}
