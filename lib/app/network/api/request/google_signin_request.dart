// To parse this JSON data, do
//
//     final googleSignInRequest = googleSignInRequestFromJson(jsonString);

import 'dart:convert';

GoogleSignInRequest googleSignInRequestFromJson(String str) => GoogleSignInRequest.fromJson(json.decode(str));

String googleSignInRequestToJson(GoogleSignInRequest data) => json.encode(data.toJson());

class GoogleSignInRequest {
  GoogleSignInRequest({
    required this.accessToken,
  });

  String accessToken;

  factory GoogleSignInRequest.fromJson(Map<String, dynamic> json) => GoogleSignInRequest(
    accessToken: json["access_token"],
  );

  Map<String, dynamic> toJson() => {
    "access_token": accessToken,
  };

  @override
  String toString() {
    return toJson().toString();
  }
}
