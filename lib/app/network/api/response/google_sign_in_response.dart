// To parse this JSON data, do
//
//     final googleSignInResponse = googleSignInResponseFromJson(jsonString);

import 'dart:convert';

GoogleSignInResponse googleSignInResponseFromJson(String str) => GoogleSignInResponse.fromJson(json.decode(str));

String googleSignInResponseToJson(GoogleSignInResponse data) => json.encode(data.toJson());

class GoogleSignInResponse {
  GoogleSignInResponse({
    required this.code,
    required this.status,
    required this.message,
    required this.data,
  });

  int code;
  String status;
  String message;
  List<Datum> data;

  factory GoogleSignInResponse.fromJson(Map<String, dynamic> json) => GoogleSignInResponse(
    code: json["code"],
    status: json["status"],
    message: json["message"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "status": status,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    required this.userId,
    required this.name,
    required this.refreshToken,
    required this.token,
    required this.expiresIn,
    required this.isNewRegister,
  });

  String userId;
  String? name;
  String refreshToken;
  String token;
  String expiresIn;
  bool isNewRegister;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    userId: json["user_id"],
    name: json["user_name"],
    refreshToken: json["refresh_token"] ?? '',
    token: json["token"],
    expiresIn: json["expires_in"],
    isNewRegister: json["isNewRegister"] ?? false,
  );

  Map<String, dynamic> toJson() => {
    "user_id": userId,
    "user_name": name,
    "refresh_token": refreshToken,
    "token": token,
    "expires_in": expiresIn,
    "isNewRegister": isNewRegister,
  };
}
