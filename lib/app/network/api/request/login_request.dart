// To parse this JSON data, do
//
//     final registerRequest = registerRequestFromJson(jsonString);

import 'dart:convert';

LoginRequest loginRequestFromJson(String str) => LoginRequest.fromJson(json.decode(str));

String registerRequestToJson(LoginRequest data) => json.encode(data.toJson());

class LoginRequest {
  LoginRequest({
    required this.email,
    required this.password,
    required this.deviceName,
  });

  String email;
  String password;
  String deviceName;

  factory LoginRequest.fromJson(Map<String, dynamic> json) => LoginRequest(
    email: json["email"],
    password: json["password"],
    deviceName: json["device_name"],
  );

  Map<String, dynamic> toJson() => {
    "email": email,
    "password": password,
    "device_name": deviceName,
  };
}
