import 'package:get/get.dart';

import 'splashscreen_logic.dart';

class SplashscreenBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SplashscreenLogic());
  }
}
