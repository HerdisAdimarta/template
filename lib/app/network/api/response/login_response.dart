// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

import 'dart:convert';

LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  int? code;
  bool? success;
  String? message;
  Data? data;

  LoginResponse({
    this.code,
    this.success,
    this.message,
    this.data,
  });

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
    code: json["code"],
    success: json["success"],
    message: json["message"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "success": success,
    "message": message,
    "data": data?.toJson(),
  };
}

class Data {
  int? idUser;
  String? namaUser;
  String? email;
  String? accessToken;
  String? emailVerifiedAt;

  Data({
    this.idUser,
    this.namaUser,
    this.email,
    this.accessToken,
    this.emailVerifiedAt,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    idUser: json["id_user"],
    namaUser: json["nama_user"],
    email: json["email"],
    accessToken: json["access_token"],
    emailVerifiedAt: json["email_verified_at"],
  );

  Map<String, dynamic> toJson() => {
    "id_user": idUser,
    "nama_user": namaUser,
    "email": email,
    "access_token": accessToken,
    "email_verified_at": emailVerifiedAt,
  };
}
