import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  const Button({
    required this.color,
    required this.text,
    required this.textColor,
    // required Function() action,
  });

  final Color color;
  final String text;
  // final Function() action;
  final Color textColor;

  get action => null;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        side: BorderSide(width: 2, color: color), backgroundColor: color,
        minimumSize: const Size(double.infinity, 5),
        padding: const EdgeInsets.symmetric(vertical: 14),
        shape: RoundedRectangleBorder( //to set border radius to button
            borderRadius: BorderRadius.circular(8)
        ),
        textStyle: const TextStyle(
          fontSize: 15.44,
          fontFamily: "Inter",
          fontWeight: FontWeight.w500,
        ),
      ),
      onPressed: () => action,
      child: Text(
        text,
        textAlign: TextAlign.left,
        style: TextStyle(
          color: textColor,
          fontFamily: "Inter",
          fontSize: 14,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }
}