import 'dart:io';

import 'package:dio/dio.dart';
import 'package:get/get.dart' hide FormData, MultipartFile;
import 'package:http_parser/http_parser.dart';
import 'package:template/app/network/api/request/google_signin_request.dart';
import 'package:template/app/network/api/request/login_request.dart';
import 'package:template/app/network/api/request/register_request.dart';
import 'package:template/app/network/api/response/google_sign_in_response.dart';
import 'package:template/app/network/api/response/login_response.dart';
import 'package:template/app/network/api/response/logout_response.dart';
import 'package:template/app/network/api/response/register_response.dart';
import 'package:template/app/network/api/response/user_response.dart';

import 'api_provider.dart';

class APIRepository {
  APIProvider apiProvider;
  APIRepository({required this.apiProvider});

  // LoginGoogle
  Future<GoogleSignInResponse?> loginGoogle({required GoogleSignInRequest data}) async {
    final res = await apiProvider
        .postData('Api/loginbyAccesToken', data)
        .catchError((onError) {
      Get.log("error : ${onError.toString()}");
    });
    Get.log('result : ${res.data}');
    return GoogleSignInResponse.fromJson(res.data);
  }

  //Login
  Future<LoginResponse?> userLogin(LoginRequest data) async {
    Get.log('API Login');
    final res = await apiProvider.postData('/api/login', FormData.fromMap(data.toJson()));
    return LoginResponse.fromJson(res.data);
  }

  //Register
  Future<RegisterResponse?> registerUser(RegisterRequest data) async {
    Get.log('API Register');
    final res = await apiProvider.postData('/api/register', FormData.fromMap(data.toJson()));
    return RegisterResponse.fromJson(res.data);
  }

  //logout
  Future<LogoutResponse?> logout() async {
    final res = await apiProvider.postLogout('/api/logout').catchError((onError) {
      Get.log("error : ${onError.toString()}");
    });
    Get.log('API Logout');
    return LogoutResponse.fromJson(res.data);
  }

  //load profile
  Future<UserResponse?> loadProfile() async {
    final res = await apiProvider.getData('/api/user').catchError((onError) {
      Get.log("error : ${onError.toString()}");
    });
    Get.log('API Profile');
    return UserResponse.fromJson(res.data);
  }

}