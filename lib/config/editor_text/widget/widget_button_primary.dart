import 'package:flutter/material.dart';

class ButtonPrimary extends StatelessWidget {
  final String buttonText;
  final Function onpressed;
  final bool isDisable;

  ButtonPrimary({
    required this.buttonText,
    required this.onpressed,
    this.isDisable = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        boxShadow: const [
          BoxShadow(
              color: Color(0xFF000000), offset: Offset(0, 4), blurRadius: 5.0)
        ],
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          stops: const [0.0, 1.0],
          colors: [
            isDisable ? const Color(0xFFD0D0D0) : const Color(0xFFB6E9E5),
            isDisable ? const Color(0xFFD0D0D0) : const Color(0xFFB6E9E5),
          ],
        ),
        color:  const Color(0xFFB6E9E5),
        borderRadius: BorderRadius.circular(5),
      ),
      child: ElevatedButton(
        style: ButtonStyle(
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
            ),
          ),
          backgroundColor: MaterialStateProperty.all(Colors.transparent),
          shadowColor: MaterialStateProperty.all(Colors.transparent),
        ),
        onPressed: () {
          if (!isDisable) {
            onpressed();
          }
        },
        child: Text(
          buttonText,
          style: const TextStyle(
            color: Colors.white,
            fontSize: 17,
          ),
        ),
      ),
    );
  }
}
