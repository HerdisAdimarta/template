import 'package:flutter/material.dart';

class ColorConstants {
  static Color lightScaffoldBackgroundColor = hexToColor('#F9F9F9');
  static Color darkScaffoldBackgroundColor = hexToColor('#2F2E2E');
  static Color secondaryAppColor = hexToColor('#EC8B8B');
  static Color secondaryDarkAppColor = Colors.white;
  static Color tipColor = hexToColor('#B6B6B6');
  static Color lightGray = Color(0XFFF6F6F6);
  static Color lightGray2 = Color(0XFFE4E7EC);
  static Color lightGray3 = Color(0XFFD7D7D7);
  static Color black = Color(0XFF000000);
  static Color black2 = Color(0XFF333333);
  static Color white = Color(0XFFFFFFFF);
  static Color mostlyWhite = Color(0XFFFBFBFB);
  static Color blue = Color(0XFF0F6FCD);
  static Color blue2 = Color(0xFF0382D5);
  static Color lightBlue = Color(0XFFEEF7FF);
  static Color darkBlue = Color(0XFF0062A2);
  static Color darkBlue2 = Color(0xFF0258AE);
  static Color darkBlue3 = Color(0xFF0470C4);
  static Color grey = Color(0XFF98A1B2);
  static Color grey2 = Color(0XFF667085);
  static Color grey3 = Color(0XFFDFDCDC);
  static Color darkGrey = Color(0XFF3F3F3F);
  static Color red = Color(0XFFD32F2F);
  static Color lightRed = Color(0XFFFF612D);
  static Color lightRed2 = Color(0XFFFFEBEE);
  static Color violet = Color(0XFF583A90);
  static Color lightGreen = Color(0XFF37C398);
  static Color lightGreen2 = Color(0XFF19DAA0);
  static Color darkGreen = Color(0XFF027A48);
  static Color cyan = Color(0XFF0ABDA7);
  static Color softCyan = Color(0XFFA4E3E7);
  static Color darkCyan = Color(0XFF6FBBC1);
  static Color magenta = Color(0XFFDA5FC6);
  static Color primaryColor = Color(0XFF0E65BB);
  static Color textColor = Color(0XFF333333);
  static Color textColor2 = Color(0XFF1D2939);
  static Color textColor3 = Color(0XFF344054);
  static Color shadowColor = Color(0X28583A90);
  static Color decorationColor = Color(0XFFECFDF3);
  static Color hintText = Color(0XFFC4C4C4);
  static Color chartDone = Color(0XFF1AE6A0);
  static Color chartOngoing = Color(0xFF1A9AFB);

}

Color hexToColor(String hex) {
  assert(RegExp(r'^#([0-9a-fA-F]{6})|([0-9a-fA-F]{8})$').hasMatch(hex),
      'hex color must be #rrggbb or #rrggbbaa');

  return Color(
    int.parse(hex.substring(1), radix: 16) +
        (hex.length == 7 ? 0xff000000 : 0x00000000),
  );
}
