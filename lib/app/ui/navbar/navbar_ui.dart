import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:template/app/ui/navbar/home/home_ui.dart';
import 'package:template/utility/shared/constants/colors.dart';

import 'navbar_logic.dart';

class NavbarUi extends StatelessWidget {
  static const String path = "/navbar";
  final logic = Get.find<NavbarLogic>();
  final state = Get.find<NavbarLogic>().state;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<NavbarLogic>(builder: (logic) {
      return WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: Scaffold(
          backgroundColor: Colors.white,
          extendBodyBehindAppBar: true,
          bottomNavigationBar: Stack(
            children: [
              BottomNavigationBar(
                showUnselectedLabels: false,
                showSelectedLabels: false,
                onTap: logic.changeTabIndex,
                currentIndex: state.selectedIndex.value,
                backgroundColor: ColorConstants.white,
                selectedItemColor: ColorConstants.violet,
                unselectedItemColor: ColorConstants.grey3,
                unselectedLabelStyle: const TextStyle(fontWeight: FontWeight.bold),
                selectedLabelStyle: const TextStyle(fontWeight: FontWeight.bold),
                type: BottomNavigationBarType.fixed,
                items: [
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.home_outlined,
                      size: 30,
                      color: ColorConstants.grey,
                    ),
                    activeIcon: Icon(
                      Icons.home_rounded,
                      size: 30,
                      color: ColorConstants.primaryColor,
                    ),
                    label: '',
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.newspaper_outlined,
                      size: 30,
                      color: ColorConstants.grey,
                    ),
                    activeIcon: Icon(
                      Icons.newspaper_rounded,
                      size: 30,
                      color: ColorConstants.primaryColor,
                    ),
                    label: '',
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.notifications_outlined,
                      size: 30,
                      color: ColorConstants.grey,
                    ),
                    activeIcon: Icon(
                      Icons.notifications_rounded,
                      size: 30,
                      color: ColorConstants.primaryColor,
                    ),
                    label: '',
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.settings_outlined,
                      size: 30,
                      color: ColorConstants.grey,
                    ),
                    activeIcon: Icon(
                      Icons.settings_rounded,
                      size: 30,
                      color: ColorConstants.primaryColor,
                    ),
                    label: '',
                  ),
                ],
              ),
            ],
          ),
          body: IndexedStack(
            index: state.selectedIndex.value,
            children: [
              HomeUi(),
              Container(),
              Container(),
              Container(),
            ],
          ),
        ),
      );
    });
  }
}
