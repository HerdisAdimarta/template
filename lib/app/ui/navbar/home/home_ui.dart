import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'home_logic.dart';

class HomeUi extends StatelessWidget {
  static const String path = '/home';
  final logic = Get.find<HomeLogic>();
  final state = Get.find<HomeLogic>().state;

  @override
  Widget build(BuildContext context) {

    return Container();
  }
}
