import 'package:get/get.dart';

import 'navbar_state.dart';

class NavbarLogic extends GetxController {
  final NavbarState state = NavbarState();

  void changeTabIndex(int index) {
    state.selectedIndex.value = index;
    Get.log('kliknya: ${state.selectedIndex.value}');
    update();
  }

}
