

import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';

class GoogleSignInService {
  final GoogleSignIn _googleSignIn = GoogleSignIn();

  Future<GoogleSignInAccount?> signInwithGoogle() async {
    if (await _googleSignIn.isSignedIn()) {
      Get.log('sdah Login');
      _googleSignIn.signOut();
    }

    final GoogleSignInAccount? googleSignInAccount = await _googleSignIn
        .signIn();
    await _googleSignIn.currentUser!.authentication.then((googleKey) {
      var idToken = _googleSignIn.currentUser!.id;
      var email = _googleSignIn.currentUser!.email;
      Get.log('idToken : $idToken');
      Get.log('email : $email');
      Get.log("User Access Token: ${googleKey.accessToken}");
      // Get.log("G-Auth Code: ${googleKey.serverAuthCode}");
      var token = googleKey.accessToken;
    });



    return googleSignInAccount;
  }

  Future<void> signOutFromGoogle() async {
    await _googleSignIn.signOut();
    // await _auth.signOut();
  }
}
