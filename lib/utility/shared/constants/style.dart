import 'package:flutter/material.dart';

class StyleConstants {
  static var stylePrimaryButton = ElevatedButton.styleFrom(
    minimumSize: const Size(double.infinity, 30),
    primary:  const Color(0xff52c4c5),
    onPrimary: Colors.white,
    padding: const EdgeInsets.symmetric(vertical: 16),
    shadowColor: const Color(0xff52c4c5),
    elevation: 6,
    enableFeedback: true,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(30),
    ),
    textStyle: const TextStyle(
      color: Colors.white,
      fontSize: 15.44,
      fontFamily: "Nunito Sans",
      fontWeight: FontWeight.w500,
    ),
  );

  static var styleSecondNegativeButton = ElevatedButton.styleFrom(
    minimumSize: const Size(double.infinity, 30),
    primary:  Colors.white,
    onPrimary: Colors.white,
    padding: const EdgeInsets.symmetric(vertical: 13),
    elevation: 1,
    enableFeedback: true,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10),
    ),
    side: const BorderSide(width: 2, color: Color(0xff52c4c5)),
    textStyle: const TextStyle(
      color: Color(0xff52c4c5),
      fontSize: 15.44,
      fontFamily: "Nunito Sans",
      fontWeight: FontWeight.w500,
    ),
  );

  static var styleSecondButton = ElevatedButton.styleFrom(
    minimumSize: const Size(double.infinity, 30),
    primary:  const Color(0xff52c4c5),
    onPrimary: Colors.white,
    padding: const EdgeInsets.symmetric(vertical: 13),
    elevation: 1,
    enableFeedback: true,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10),
    ),
    textStyle: const TextStyle(
      color: Colors.white,
      fontSize: 15.44,
      fontFamily: "Nunito Sans",
      fontWeight: FontWeight.w500,
    ),
  );

  static const heading1Text = TextStyle(
    color: Color(0xff333333),
    fontSize: 14,
    fontFamily: "Nunito Sans",
    fontWeight: FontWeight.w700,
  );

  static const appBarText = TextStyle(
    color: Color(0xff333333),
    fontSize: 18,
    fontFamily: "Nunito Sans",
    fontWeight: FontWeight.w700,
  );
}