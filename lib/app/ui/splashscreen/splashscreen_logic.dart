import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:template/app/ui/login/login_ui.dart';
import 'package:template/utility/shared/constants/storage.dart';

import 'splashscreen_state.dart';

class SplashscreenLogic extends GetxController {
  final SplashscreenState state = SplashscreenState();

  final prefs = Get.find<SharedPreferences>();

  @override
  Future<void> onInit() async {
    Future.delayed(const Duration(seconds: 5), () {
      Get.toNamed(LoginUi.path);
    });
    // requestCameraPermission();
    update();
    super.onInit();
  }
  Future<void> requestCameraPermission() async {
    final status = await Permission.camera.request();
    if (status == PermissionStatus.granted) {
      // Permission granted.
      Future.delayed(const Duration(seconds: 5), () {
        Get.toNamed(LoginUi.path);
      });
    } else if (status == PermissionStatus.denied) {
      // Permission denied.
      requestCameraPermission();
    } else if (status == PermissionStatus.permanentlyDenied) {
      // Permission permanently denied.
      requestCameraPermission();
    }
  }
}
