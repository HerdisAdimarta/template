import 'package:get/get.dart';
import 'package:template/app/ui/login/login_binding.dart';
import 'package:template/app/ui/login/login_ui.dart';
import 'package:template/app/ui/navbar/home/home_binding.dart';
import 'package:template/app/ui/navbar/home/home_ui.dart';
import 'package:template/app/ui/navbar/navbar_ui.dart';
import 'package:template/app/ui/splashscreen/splashscreen_binding.dart';
import 'package:template/app/ui/splashscreen/splashscreen_ui.dart';

class AppPages {
  static const initial = SplashscreenUi.path;

  static final routes = [
    GetPage(
      name: SplashscreenUi.path,
      page: () => SplashscreenUi(),
      binding: SplashscreenBinding(),
    ),
    GetPage(
      name: LoginUi.path,
      page: () => LoginUi(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: NavbarUi.path,
      page: () => NavbarUi(),
      transition: Transition.fade,
        bindings: [
          HomeBinding(),
          // HomeBinding(),
          // HomeBinding(),
        ]
    ),
    GetPage(
      name: HomeUi.path,
      page: () => HomeUi(),
      binding: HomeBinding(),
    ),
  ];
}
