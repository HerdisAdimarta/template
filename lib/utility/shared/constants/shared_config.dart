import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:template/config/editor_text/widget/custom_widget_dialog.dart';

class Config {
  static const notif = 'wow1';
  static const database = "databasewow1.db";
  bool validateBool(dynamic value) {
    if (value == null) {
      return false;
    } else {
      return value as bool;
    }
  }

  printPage(String value) async {
    Get.log("Page Index : $value");
  }

  Future<bool> isRestart() async {
    if (await Datas().getString(Datas.restart) == "") {
      return false;
    } else {
      return true;
    }
  }

  Future showLoading(BuildContext context) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return CustomAlert();
      },
    );
  }

  Future<Options> getOptions() async {
    var options = Options(
        validateStatus: (status) {
          return status! < 499;
        },
        headers: {
          'Authorization': 'Bearer ${await Datas().getString(Datas.token)}'
        });
    return options;
  }

  var appBarCustom = PreferredSize(
    preferredSize: const Size.fromHeight(0.0),
    child: AppBar(
      elevation: 0,
      backgroundColor: const Color(0xFFB6E9E5), systemOverlayStyle: SystemUiOverlayStyle.light,
    ),
  );

  var appBarCustomWhite = PreferredSize(
    preferredSize: const Size.fromHeight(0.0),
    child: AppBar(
      elevation: 0,
      backgroundColor: const Color(0xFFB6E9E5), systemOverlayStyle: SystemUiOverlayStyle.light,
    ),
  );

  static const defaultMarginSide = 20.0;
  static const appName = 'GarudaTekno';
}

class ErrorDio {
  static const logout =
      'Your Account Has Been Login From Another Device. Please Relogin';
  static const server = 'Something Error';
  static const timeOut = 'Something Error. Please Try Again';
}

class Message {}

class MyRoute extends MaterialPageRoute {
  MyRoute({required super.builder});

  @override
  Duration get transitionDuration => const Duration(seconds: 0);
}

class Datas {
  static const isLogut = 'isLogut';
  static const msgLogut = 'msgLogut';

  static const token = 'token';
  static const refresh = 'refresh';
  static const takeFotoFisik = 'takeFotoFisik';
  static const imeiImage = 'imeiImage';
  static const fullSetImage = 'fullSetImage';
  static const restart = 'restart';
  //
  removeData() {
    // OneSignal.shared.removeExternalUserId();
    removeReference(token);
    removeReference(refresh);
  }

  removeReference(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }

  setString(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  Future<String> getString(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString(key) == null) {
      return '';
    } else {
      return prefs.getString(key) ?? "";
    }
  }

}