import 'dart:convert';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio/dio.dart';
import 'package:dio/io.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Response, FormData;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:template/app/network/api/api_repository.dart';
import 'package:template/config/core/env.dart';
import 'package:template/utility/log/dio_logger.dart';
import 'package:template/utility/shared/constants/storage.dart';

class APIProvider {
  static const String tag = 'APIProvider';
  // static const String URL_API_DAERAH_INDONESIA =
  //     "https://dev.farizdotid.com/api/daerahindonesia";

  static final String _baseUrl = Env.value.baseUrl;

  late bool isConnected = false;
  late Dio _dio;
  var tokenDio = Dio();
  late BaseOptions dioOptions;
  var storage1 = Get.find<SharedPreferences>();
  // final storage2 = GetStorage();
  late APIRepository apiRepository;

  APIProvider() {
    dioOptions = BaseOptions()..baseUrl = APIProvider._baseUrl;
    dioOptions.validateStatus = (value) {
      return value! < 500;
    };

    _dio = Dio(dioOptions);
    var cookieJar = CookieJar();
    _dio.interceptors.add(CookieManager(cookieJar));
    tokenDio.options = _dio.options;

    _dio.interceptors
        .add(InterceptorsWrapper(onRequest: (options, handler) async {
      DioLogger.onSend(tag, options);
      await checkConnectivity();
      handler.next(options);
    }, onResponse: (response, handler) {
      DioLogger.onSuccess(tag, response);
      return handler.next(response);
    }, onError: (DioException dioError, handler) {
      DioLogger.onError(tag, dioError);

      throwIfNoSuccess(dioError);
      return handler.next(dioError);
    }));

    // if you use ip dns server
    _dio.httpClientAdapter = IOHttpClientAdapter(
      createHttpClient: () {
        // Don't trust any certificate just because their root cert is trusted.
        final HttpClient client = HttpClient(context: SecurityContext(withTrustedRoots: false));
        // You can test the intermediate / root cert here. We just ignore it.
        client.badCertificateCallback = (cert, host, port) => true;
        return client;
      },
    );
  }

  Future<Response> postData(String path, dynamic data) async {
    try {
      var response = await _dio.post(path, data: data);
      return response;
    } on DioException catch (ex) {
      throw Exception(json.decode(ex.response.toString())["error"]);
    }
  }

  Future<Response> postDataGoogle(String path, dynamic data) async {
    try {
      await addHeaderGoogle();
      var response = await _dio.post(path, data: data);
      return response;
    } on DioException catch (ex) {
      throw Exception(json.decode(ex.response.toString())["error"]);
    }
  }

  Future<Response> postDataHeader1(String path, dynamic data) async {
    try {
      await addHeader();
      var response = await _dio.post(path, data: data);
      return response;
    } on DioException catch (ex) {
      throw Exception(json.decode(ex.response.toString())["error"]);
    }
  }

  Future<Response> postMediaHeader(String path, dynamic data,
      {required String titleProgress}) async {
    try {
      await addHeader();
      var response = await _dio.post(path, data: data,
          onSendProgress: (int sent, int total) {
            Get.log("Uploading ${(sent / total * 100)}%");
            var percentage = sent / total * 100;
            // if (percentage < 98) {
            //   EasyLoading.showProgress(percentage / 100,
            //       status: '$titleProgress... ${percentage.toInt()}%');
            // }
          }, onReceiveProgress: (count, total) {
            // EasyLoading.showProgress(100 / 100,
            //     status: '$titleProgress... ${100}%');
            // EasyLoading.dismiss();
          });
      return response;
    } on DioException catch (ex) {
      throw Exception(json.decode(ex.response.toString())["error"]);
    }
  }

  Future<Response> postMedia(String path, dynamic data,
      {required String titleProgress}) async {
    try {
      var response = await _dio.post(path, data: data,
          onSendProgress: (int sent, int total) {
            Get.log("Uploading ${(sent / total * 100)}%");
            var percentage = sent / total * 100;
            // if (percentage < 98) {
            //   EasyLoading.showProgress(percentage / 100,
            //       status: '$titleProgress... ${percentage.toInt()}%');
            // }
          }, onReceiveProgress: (count, total) {
            // EasyLoading.showProgress(100 / 100,
            //     status: '$titleProgress... ${100}%');
            // EasyLoading.dismiss();
          });
      return response;
    } on DioException catch (ex) {
      throw Exception(json.decode(ex.response.toString())["error"]);
    }
  }

  Future<Response> getData(String path) async {
    try {
      await addHeader();
      var response = await _dio.get(path);
      return response;
    } on DioException catch (ex) {
      throw  Exception(json.decode(ex.response.toString())["error"]);
    }
  }

  Future<Response> postLogout(String path) async {
    try {
      await addHeader();
      var response = await _dio.post(path);
      return response;
    } on DioException catch (ex) {
      throw  Exception(json.decode(ex.response.toString())["error"]);
    }
  }

  Future<Response> getDataWithHeader(
      {required String path, required Map<String, dynamic> params}) async {
    try {
      await addHeader();
      var response = await _dio.get(path, queryParameters: params);
      return response;
    } on DioException catch (ex) {
      throw Exception(json.decode(ex.response.toString())["error"]);
    }
  }

  Future<Response> getDataParams(
      {required String path, required Map<String, dynamic> params}) async {
    try {
      var response = await _dio.get(path, queryParameters: params);
      return response;
    } on DioException catch (ex) {
      throw Exception(json.decode(ex.response.toString())["error"]);
    }
  }

  void throwIfNoSuccess(DioException ex) async {
    if (ex.response!.statusCode! < 200 || ex.response!.statusCode! > 299) {
      Get.log("Gagal Oy");
      String errorMessage = json.decode(ex.response.toString())["error"] ??
          json.decode(ex.response.toString())["message"];
      Get.snackbar(
        'Oops..',
        errorMessage,
        backgroundColor: const Color(0xFF3F4E61),
      );
      throw  Exception(errorMessage);
    }
  }

  Future<BaseOptions> addHeader() async {
    dioOptions.headers = {
      'Authorization': 'Bearer ${storage1.getString(StorageConstants.token)}',
      // 'content-type': 'multipart/form-data'
      // 'Authorization': '${box.read('authKey')}'
    };

    Get.log('header : ${dioOptions.headers}');
    return dioOptions;
  }
  Future<BaseOptions> addHeaderGoogle() async {
    dioOptions.headers = {
      'Authorization': '${storage1.getString(StorageConstants.token)}',
      // 'content-type': 'multipart/form-data'
      // 'Authorization': '${box.read('authKey')}'
    };

    Get.log('header : ${dioOptions.headers}');
    return dioOptions;
  }

  Future<BaseOptions> urlCustomOpt(String url) async {
    dioOptions.baseUrl = url;
    return dioOptions;
  }

  Future<BaseOptions> urlDefaultOpt() async {
    dioOptions.baseUrl = _baseUrl;
    return dioOptions;
  }

  noInternetWarning() async {
    await Get.defaultDialog(
      title: "No Internet",
      titlePadding: const EdgeInsets.all(20),
      titleStyle: const TextStyle(fontSize: 14),
      contentPadding: const EdgeInsets.only(bottom: 20, left: 14, right: 14),
      middleText: "Please check your connectivity!",
      middleTextStyle: const TextStyle(
        fontSize: 10,
      ),
      confirm: ElevatedButton(
        onPressed: () => Get.back(),
        style: ElevatedButton.styleFrom(
          foregroundColor: Colors.white, backgroundColor: Colors.green,
          shadowColor: Colors.transparent,
          textStyle: const TextStyle(
            color: Colors.white,
            fontSize: 12.44,
            fontFamily: "Poppins",
            fontWeight: FontWeight.w500,
          ),
        ),
        child: const Text("Try Again"),
      ),
      cancel: ElevatedButton(
        onPressed: () => Get.back(),
        style: ElevatedButton.styleFrom(
          foregroundColor: Colors.white, backgroundColor: Colors.red,
          shadowColor: Colors.transparent,
          textStyle: const TextStyle(
            color: Colors.white,
            fontSize: 12.44,
            fontFamily: "Poppins",
            fontWeight: FontWeight.w500,
          ),
        ),
        child: const Text("Close"),
      ),
    );
  }

  checkConnectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      Get.back();
      isConnected = false;
      noInternetWarning();
    } else {
      isConnected = true;
    }
  }
}
