import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ButtonFail extends StatelessWidget {
  final String buttonText;
  final Function onpressed;

  ButtonFail({
    required this.buttonText,
    required this.onpressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(5.0))),
      child: TextButton(
        style: ButtonStyle(
          padding: MaterialStateProperty.all<EdgeInsets>(const EdgeInsets.all(0)),
          foregroundColor: MaterialStateProperty.all<Color>(const Color(0xFFFF3333)),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                  side: const BorderSide(color: Color(0xFFFF3333)))),
        ),
        onPressed: () {
          onpressed();
        },
        child: Text(
          buttonText,
          style: const TextStyle(
            color: Color(0xFFFF3333),
            fontWeight: FontWeight.bold,
            fontSize: 17,
          ),
        ),
      ),
    );
  }
}
