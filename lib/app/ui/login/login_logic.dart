import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:template/app/network/api/api_repository.dart';
import 'package:template/app/network/api/request/login_request.dart';
import 'package:template/app/ui/navbar/navbar_ui.dart';
import 'package:template/utility/shared/constants/colors.dart';
import 'package:template/utility/shared/constants/shared_config.dart';
import 'package:template/utility/shared/constants/storage.dart';

import 'login_state.dart';

class LoginLogic extends GetxController {
  final LoginState state = LoginState();

  LoginLogic({required this.apiRepository});
  APIRepository apiRepository;

  var prefs = Get.find<SharedPreferences>();
  var storage = Get.find<SharedPreferences>();

  var emailCtrl = TextEditingController();
  var passCtrl = TextEditingController();

  late GoogleSignIn _googleSignIn;
  GoogleSignInAccount? _currentUser;

  Future<void> signIn() async {
    try {
      final auth = await _googleSignIn.signIn();
      _currentUser = auth;
    } catch (e) {
      print('Error signing in $e');
    }
  }

  Future<void> toggleVisible() async {
    state.isPasswordHidden.value = !state.isPasswordHidden.value;
    update();
  }

  void loginByEmail( context) async {
    if (emailCtrl.text.isEmpty) {
      Get.snackbar('Failed', 'Silahkan masukan username Anda', colorText: Colors.white, backgroundColor: Colors.redAccent);
      return;
    } else if (passCtrl.text.isEmpty) {
      Get.snackbar('Failed', 'Silahkan masukan kata sandi Anda', colorText: Colors.white, backgroundColor: Colors.redAccent);
      return;
    }
    Get.toNamed(NavbarUi.path);

    // Config().showLoading(context);
    // Get.log("LOADING");
    // Get.log("request: username: ${emailCtrl.text}, password: ${passCtrl.text}");
    // final res = await apiRepository.userLogin(LoginRequest(
    //     email: emailCtrl.text,
    //     password: passCtrl.text,
    //     deviceName: 'test'));
    //
    // Get.log('$res');
    // if (res == null) {
    //   Get.log("GAGAL2");
    //   return;
    // } else {
    //   Get.back();
    //
    //   if (res.code == 200) { //berhasil masuk ke page berikutnya
    //     Get.log("MASUK");
    //     prefs.setString(StorageConstants.token, '${res.data?.accessToken}');
    //     // Get.toNamed(NavbarUi.path, parameters: {
    //     //   'token': '${res.data?.accessToken}',
    //     // });
    //   } else {
    //     Get.snackbar('Failed ${res.code}', '${res.message}', colorText: ColorConstants.white, backgroundColor: Colors.redAccent);
    //
    //   }
    //
    // }
  }
}
