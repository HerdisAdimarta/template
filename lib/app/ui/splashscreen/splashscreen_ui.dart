import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:template/utility/shared/utils/common_widget.dart';

import 'splashscreen_logic.dart';

class SplashscreenUi extends StatelessWidget {
  static const String path = '/splashscreen';
  final logic = Get.find<SplashscreenLogic>();
  final state = Get.find<SplashscreenLogic>().state;

  @override
  Widget build(BuildContext context) {
    CommonWidget.actionBarTransparant();
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          color: Colors.white,
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                  child: Image.asset(
                    "assets/ic_splashscreen.png",
                    width: 200,
                    height: 200,
                  ),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}
