import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:template/utility/shared/constants/colors.dart';
import 'package:template/utility/shared/constants/shared_config.dart';
import 'package:template/utility/shared/utils/common_widget.dart';

import 'login_logic.dart';

class LoginUi extends StatelessWidget {
  static const String path = '/login';
  final logic = Get.find<LoginLogic>();
  final state = Get.find<LoginLogic>().state;

  @override
  Widget build(BuildContext context) {
    CommonWidget.actionBarLigth();
    Size size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: GetBuilder<LoginLogic>(builder: (logic) {
            return Column(
              children: [
                const SizedBox(height: 80),
                topPanel(size),
                loginEmail(context),
                // loginPhone(context),
                const SizedBox(height: 15),
                // googleSignUI(context),
                const SizedBox(height: 15),
              ],
            );
          }),
        ),
      ),
    );
  }

  Widget topPanel(Size size) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Column(
          children: [
            Image.asset(
              "assets/ic_splashscreen.png",
              width: 180,
            ),
          ],
        ),
      ],
    );
  }

  Widget loginEmail(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 25.0, left: 25.0, top: 10),
      child: Container(
        margin: const EdgeInsets.only(
            top: Config.defaultMarginSide,
            bottom: Config.defaultMarginSide),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'Email',
              style: TextStyle(
                color: Colors.black,
                fontSize: 14,
                fontFamily: 'Inter',
                fontWeight: FontWeight.w400,
              ),
            ),
            const SizedBox(height: 10),
            Container(
              padding: const EdgeInsets.only(
                  left: 20.0,
                  right: 20.0, top: 5.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(13),
                boxShadow: [
                  BoxShadow(
                    color: ColorConstants.shadowColor,
                    blurRadius: 8,
                    offset: const Offset(0, 4),
                  ),
                ],
                color: Colors.white,
              ),
              child: TextField(
                enableSuggestions: false,
                autocorrect: false,
                keyboardType: TextInputType.emailAddress,
                textInputAction: TextInputAction.done,
                controller: logic.emailCtrl,
                style: TextStyle(
                  color: ColorConstants.darkGrey,
                  fontSize: 16,
                ),
                decoration: InputDecoration(
                    contentPadding:
                    const EdgeInsets.only(bottom: 5),
                    // hintText: "Email",
                    hintStyle: TextStyle(
                      color: ColorConstants.hintText,
                      fontSize: 16,
                    ),
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              'Password',
              style: TextStyle(
                color: Colors.black,
                fontSize: 14,
                fontFamily: 'Inter',
                fontWeight: FontWeight.w400,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              padding: const EdgeInsets.only(
                  left: 20.0,
                  right: 20.0, top: 5.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(13),
                boxShadow: [
                  BoxShadow(
                    color: ColorConstants.shadowColor,
                    blurRadius: 8,
                    offset: const Offset(0, 4),
                  ),
                ],
                color: Colors.white,
              ),
              child: Obx(() {
                return TextFormField(
                  obscureText:
                  state.isPasswordHidden.value,
                  decoration: InputDecoration(
                    // hintText: "Password",
                      hintStyle: TextStyle(
                        color: ColorConstants.hintText,
                        fontSize: 16,
                      ),
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      suffixIcon: IconButton(
                          icon: Icon(state.isPasswordHidden.value ? Icons
                              .visibility_off : Icons.visibility),
                          onPressed: () {
                            logic.toggleVisible();
                          })),
                  enableSuggestions: false,
                  autocorrect: false,
                  textInputAction: TextInputAction.done,
                  controller: logic.passCtrl,
                  style: TextStyle(
                    color: ColorConstants.darkGrey,
                    fontSize: 16,
                  ),
                );
              }),
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                side: BorderSide(width: 2, color: ColorConstants.primaryColor), backgroundColor: ColorConstants.primaryColor,
                minimumSize: const Size(double.infinity, 5),
                padding: const EdgeInsets.symmetric(vertical: 14),
                shape: RoundedRectangleBorder( //to set border radius to button
                    borderRadius: BorderRadius.circular(8)
                ),
                textStyle: const TextStyle(
                  fontSize: 15.44,
                  fontFamily: "Inter",
                  fontWeight: FontWeight.w500,
                ),
              ),
              onPressed: () => logic.loginByEmail(context),
              // onPressed: () => Get.toNamed(NavbarUi.path),
              child: const Text(
                'Masuk',
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: "Inter",
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container googleSignUI(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(right: 10.0, left: 10.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: double.infinity,
              decoration: BoxDecoration(
                color: ColorConstants.white,
                borderRadius: BorderRadius.circular(30),
              ),
              child: ElevatedButton.icon(
                icon: Container(
                  alignment: Alignment.center,
                  child: Image.asset(
                    'assets/google.png',
                    height: 22.0,
                    fit: BoxFit.cover,
                  ),
                ),
                label: Text("Lanjutkan dengan Google",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.nunitoSans(
                      color: const Color(0xff333333),
                      fontSize: 14,
                      fontWeight: FontWeight.w700,
                      letterSpacing: 0.42,
                    )),
                onPressed: () => {},
                style: ElevatedButton.styleFrom(
                  foregroundColor: ColorConstants.black, backgroundColor: ColorConstants.white, minimumSize: const Size(double.infinity, 30),
                  padding: const EdgeInsets.symmetric(vertical: 16),
                  shadowColor: ColorConstants.primaryColor,
                  elevation: 6,
                  enableFeedback: true,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

}
