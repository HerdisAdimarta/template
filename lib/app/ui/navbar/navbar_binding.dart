import 'package:get/get.dart';

import 'navbar_logic.dart';

class NavbarBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => NavbarLogic());
  }
}
