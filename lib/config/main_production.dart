import 'package:flutter/material.dart';
import 'package:template/config/core/env.dart';

void main() => Production();

class Production extends Env {
  final String appName = 'template apps';
  final String baseUrl = 'https://google.com';
  final Color primarySwatch = Colors.teal;
  EnvType environmentType = EnvType.PRODUCTION;

  final String dbName = 'template.db';

}