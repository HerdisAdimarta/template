import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ButtonSecondary extends StatelessWidget {
  final String buttonText;
  final Function onpressed;

  ButtonSecondary({
    required this.buttonText,
    required this.onpressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(5.0))),
      child: TextButton(
          style: ButtonStyle(
              padding: MaterialStateProperty.all<EdgeInsets>(const EdgeInsets.all(0)),
              foregroundColor:
              MaterialStateProperty.all<Color>(const Color(0xFFB6E9E5)),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      side: const BorderSide(color: Color(0xFFB6E9E5))))),
          onPressed: () {
            onpressed();
          },
          child: Text(
            buttonText,
            style: const TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 17,
            ),
          )),
    );
  }
}
