import 'package:get/get.dart';
import 'package:template/app/ui/navbar/navbar_logic.dart';

import 'home_logic.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HomeLogic());
    Get.lazyPut(() => NavbarLogic());
  }
}
